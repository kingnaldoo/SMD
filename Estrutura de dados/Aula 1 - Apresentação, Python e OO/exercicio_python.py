"""Utilize este arquivo para realizar as implementações dos exercícios abaixo.
Todos foram retirados de nosso livro texto"""
from random import randrange, choice

def is_multiple(n, m):
    return n % m == 0 if True else False

def minmax(data):
    data.sort()
    return [data[0], data[len(data)-1]]

def squares_sum(n):
    soma = 0
    for i in range(0, n+1):
        soma+=i**2
    return soma

def squares_sum_2(n):
    return sum(map(lambda x: x**2, n))

def squares_sum_odd(n):
    soma = 0
    for i in range(0, n+1):
        if(i % 2 != 0):
            soma+=i**2
    return soma

def squares_sum_odd_2(n):
    return sum(map(lambda x: x % 2 != 0 and x**2, n))

def my_choice(data):
    return data[randrange(0, len(data)+1)]

def distinct(data):
    for i in data: 
        if data.count(i) != 1: return False
    return True

def my_list():
    list = []
    soma = acc = 0
    for i in range(0, 10):
        soma += acc
        acc+=2
        list.append(soma)
    return list

def number_of_vowels(s):
    count = 0
    for i in s:
        if i == 'a' or i == 'e' or i == 'i' or i == 'o' or i == 'u':
            count+=1
    return count

def only_words(sentence):
    new_sentence = []
    for char in sentence: 
        if char.isalnum() or char == ' ':
            new_sentence.append(char)
    return ''.join(new_sentence)
    
def correct_arithmetic():
    a = int(input('Digite um número inteiro: '))
    b = int(input('Digite outro número: '))
    c = int(input('Só mais um: '))

    if(a + b == c or a == b - c or a * b == c):
        return True
    return False

# Bônus
def school_pubishment():
    for i in range(1, 101):
        phrase = 'Eu nunca mais vou enviar spam para meus amigos novamente.'
        phrase = list(phrase)
        phrase[randrange(0, len(phrase))] = choice(['a', 'c', 't', 'l', 'f', 'e', 'p', 'h'])
        phrase = ''.join(phrase)
        print(f'{i} - {phrase}')

school_pubishment()