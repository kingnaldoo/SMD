package aula1;

public class Principal {
    public static void main(String[] args) {
        Carro carroJoao = new Carro(4, 4, "vermelho"); //contrutor

        carroJoao.quantidadePneus = 4;
        carroJoao.quantidadePortas = 4;
        carroJoao.cor = "vermelho";

        Carro carroMaria = new Carro(4, 5, "amarelo");

        carroMaria.acelerar();
        carroMaria.acelerar();

        carroMaria.frear();
        carroMaria.frear();
        
        System.out.println(carroJoao);
        System.out.println(carroMaria);

        carroJoao = null;

        System.gc(); //garbage colletor
    }    
}
