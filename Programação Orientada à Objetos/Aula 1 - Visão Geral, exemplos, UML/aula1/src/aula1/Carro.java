package aula1;

public class Carro {
    int quantidadePneus;
    int quantidadePortas;
    int velocidade;
    String cor;

    

    public Carro(int quantidadePneus, int quantidadePortas, String cor) {
        this.quantidadePneus = quantidadePneus;
        this.quantidadePortas = quantidadePortas;
        this.cor = cor;
    }

    void acelerar() {
        if (this.velocidade != 0) {
            System.out.println("Freando");
            this.velocidade = 0;
        }
    }

    void frear() {
        if (this.velocidade == 0) {
            System.out.println("Acelerando");
            this.velocidade = this.velocidade + 10;
        }
    }
}
