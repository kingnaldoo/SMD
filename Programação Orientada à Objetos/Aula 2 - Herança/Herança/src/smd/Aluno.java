package smd;

public class Aluno extends Pessoa {
    /// não pode ser nulo
    private Pessoa responsavel;

    /**
     *
     * @param responsavel Responsável pelo aluno. Não pode ser nulo!
     */
    public Aluno(String nome, Pessoa responsavel) {
        super(nome); // o aluno é uma Pessoa com nome "tal"

        this.responsavel = responsavel;
    }

    public Pessoa getResponsavel() {
        return responsavel;
    }

    public void setResponsavel(Pessoa p) {
        this.responsavel = p;
    }

}