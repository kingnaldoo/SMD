package smd;

public class Pessoa { // line saver

    private String nome;

    public Pessoa(String name) {
        this.nome = name;
    }

    @Override
    public String toString() {
        return nome;
    }

    public static void main(String[] args) {
        Pessoa c = new Pessoa("Chico");
        Aluno a = new Aluno("Stu", c); //

        System.out.println(a.getResponsavel());

        a.setResponsavel(new Pessoa("José"));

        System.out.println(a);
        System.out.println(a.getResponsavel());
    }
}